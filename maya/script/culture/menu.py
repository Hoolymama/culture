import pymel.core as pm

import os
import glob
 

MAYA_PARENT_WINDOW = "MayaWindow"
HOOLY_MENU = "HoolyMenu"

def ensure_hooly_menu():
    menu = next((m for m in pm.lsUI(menus=True) if m.getLabel() == "Hooly"), None)
    if menu:
        return menu
    return pm.menu(HOOLY_MENU, label="Hooly", tearOff=True, parent=MAYA_PARENT_WINDOW)
 

class CultureMenu(object):
    def __init__(self):
        self.hooly_menu = ensure_hooly_menu()
        pm.setParent(self.hooly_menu, menu=True)
        
        pm.menuItem(label="Culture", subMenu=True)

        pm.menuItem(
        label="Create Color Sampler",
        command=pm.Callback(create_color_sampler)
    )

def create_color_sampler():
    pm.mel.source("createColorSampler")
    pm.mel.eval( 'createColorSampler()' )



