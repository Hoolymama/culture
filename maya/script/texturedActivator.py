"""this_is_a_one_liner."""
import pymel.core as pm

def create_locator(node):
    loc = pm.spaceLocator()
    loc.rename('textureActivatorPosLocator#')
    pm.parent(loc, node, r=True)
    # pm.addAttr(node, at="message", ln="textureActivatorLocator")
    # loc.attr('message') >> node.attr('textureActivatorLocator')
    return loc

# def create_locators():
#     sel = pm.ls(selection=True, transforms=True)
#     for node in nodes:
#         create_locator(node)

def create(lookup_type):
    """this_is_a_one_liner."""
    nodes = pm.ls(selection=True, transforms=True)
    
    try:
        pm.loadPlugin("Flock")
        pm.loadPlugin("Culture")
        pm.loadPlugin("mtoa")
    except:
        print "Need Flock, Culture, and mtoa plugins";
        return

    lookup_type = ('lookupPP','rampLookupPP')[lookup_type]
    
    psys = particle_pivots(nodes)

    sampler, proj, place3d = create_color_network(psys, lookup_type=lookup_type)

    a2m, switch = connect_a2m_rgb_ai_user_color(psys, nodes)

def particle_pivots(nodes):
    """this_is_a_one_liner."""
    pos = []
    locs = []
    for node in nodes:
        loc = None
        try:
            loc = pm.PyNode(node.attr("textureActivatorLocator").inputs()[0])
        except:
            loc = create_locator(node)

        rpiv = loc.getRotatePivot(space='world')
        pos.append(rpiv)
        locs.append(loc)

    crv = pm.curve(degree=1, p=pos)
    part = pm.nParticle(p=pos)[1]
    crv.getChildren()[0].attr('worldSpace[0]') >>  part.attr('goalGeometry[0]')
    i = 0
    for loc in locs:
        loc.getChildren()[0].attr('worldPosition') >>  crv.getChildren()[0].attr('controlPoints[%d]' % i)
        i+=1
    return part


def create_color_network(psys, lookup_type='rampLookupPP'):
    """this_is_a_one_liner."""

    pm.addAttr(psys, ln="rgbPP", dt="vectorArray")
    pm.addAttr(psys, ln="rgbPP0", dt="vectorArray")
    pm.addAttr(psys, ln="envelopeTimePP", dt="doubleArray")

    sampler = pm.general.createNode('colorSampler')

    lookup_in_attr = None
    lookup_out_attr = None
    if lookup_type == 'rampLookupPP':
        lookup = pm.createNode('rampLookupPP')
        lookup_in_attr = lookup.attr('input')
        lookup_out_attr = lookup.attr('output')

        lookup.attr("ramp[0].ramp_FloatValue").set(0)
        lookup.attr("ramp[0].ramp_Position").set(0)
        lookup.attr("ramp[0].ramp_Interp").set(1)
        lookup.attr("ramp[1].ramp_FloatValue").set(1)
        lookup.attr("ramp[1].ramp_Position").set(0.5)
        lookup.attr("ramp[1].ramp_Interp").set(1)
        lookup.attr("ramp[2].ramp_FloatValue").set(0)
        lookup.attr("ramp[2].ramp_Position").set(1)
        lookup.attr("ramp[2].ramp_Interp").set(1)
        lookup.attr("inputMax").set(10)

    else:
        lookup = pm.createNode('lookupPP')
        lookup_in_attr = lookup.attr('input')
        lookup_out_attr = lookup.attr('outputD')
        anicrv = pm.createNode('animCurveUU')
        pm.setKeyframe(anicrv, f=0,v=0)
        pm.setKeyframe(anicrv, f=5,v=1)
        pm.setKeyframe(anicrv, f=10,v=0)
        anicrv.attr('output') >> lookup.attr('functionCurve')

    psys.attr('envelopeTimePP') >> lookup_in_attr
    psys.attr('particleRenderType').set(4) 

    c_expr = pm.dynExpression(psys, creation=True, query=True)
    if c_expr:
        c_expr += ";\n"
    c_expr += "rgbPP=0;\nenvelopeTimePP = 0;\n"
    
    rbd_expr = pm.dynExpression(psys, rbd=True, query=True)
    if rbd_expr:
        rbd_expr += ";\n"
    # rbd_expr += "rgbPP = " + sampler.name() + ".outColor;\n"
    rbd_expr += "if ((envelopeTimePP > 0) || (%s.outRed > 0.5)) { \n" % sampler
    rbd_expr += "envelopeTimePP += 1;\n}\n"
    rbd_expr += "rgbPP = %s;\n" % lookup_out_attr

    psys.attr('worldPosition') >> sampler.attr('points')
    psys.attr('worldPosition') >> sampler.attr('refPoints')

    proj = pm.shadingNode('projection', asTexture=True)
    place3d = pm.shadingNode('place3dTexture', asUtility=True)

    place3d.attr('wim[0]') >> proj.attr('pm')
    proj.attr('outColor') >> sampler.attr('color')
    proj.attr('defaultColor').set(0, 0, 0)
    proj.attr('image').set(1, 1, 1)
    proj.attr('wrap').set(0)

    srcAtt = psys.attr('internalColorRamp') 
    destAtt = psys.attr('rgbPP') 

    cmd = 'import pymel.core as pm;pm.disconnectAttr("%s","%s");pm.dynExpression("%s", s="%s", rbd=True)' % (srcAtt,destAtt,psys, rbd_expr.encode('unicode-escape'))
    pm.evalDeferred(cmd, lp=True )
    cmd = 'import pymel.core as pm;pm.dynExpression("%s", s="%s", creation=True)' % (psys, c_expr.encode('unicode-escape'))
    pm.evalDeferred(cmd, lp=True )

    return (sampler, proj, place3d)

def connect_a2m_rgb_ai_user_color(psys, nodes):

    a2m = pm.general.createNode('arrayToMulti')

    a2mOut = 'outputsV'
    psys.attr("rgbPP") >> a2m.attr('input')
    switch = pm.createNode("aiUserDataColor")
    pm.Attribute("%s.colorAttrName" % switch).set('color')
    standard = pm.shadingNode("aiStandard", asShader=True)
    standard.rename('aiStandardTextureActivator')
    switch.attr('outColor') >> standard.attr('color')

    i = 0
    for node in nodes:
        node = pm.PyNode(node)
        shape = pm.listRelatives(node, shapes=True, f=True)[0]
        pm.addAttr(shape, at="double3", ln="mtoa_constant_color")
        pm.addAttr(shape, at="double", ln="mtoa_constant_colorX", parent="mtoa_constant_color")
        pm.addAttr(shape, at="double", ln="mtoa_constant_colorY", parent="mtoa_constant_color")
        pm.addAttr(shape, at="double", ln="mtoa_constant_colorZ", parent="mtoa_constant_color")

        destAttr = "%s.mtoa_constant_color" % (shape)

        a2m.attr(a2mOut)[i] >> destAttr

        i += 1

    return (a2m, switch)


#### EXPRESSION FOR PARTICLE DECAY

# float $decayX = nParticleShape1.colorDecayX;
# float $decayY = nParticleShape1.colorDecayY;
# float $decayZ = nParticleShape1.colorDecayZ;


# vector $col = colorSampler1.outColor;
# float $rgb[] = nParticleShape1.rgbPP;
# if (($col.x) > ($rgb[0])) {
#     $rgb[0] = $col.x;
# } else {
#     $rgb[0] = $rgb[0] - $decayX;
# }
# if (($col.y) > ($rgb[1])) {
#     $rgb[1] = $col.y;
# } else {
#     $rgb[1] = $rgb[1] - $decayY;
# }
# if (($col.z) > ($rgb[2])) {
#     $rgb[2] = $col.z;
# } else {
#     $rgb[2] = $rgb[2] - $decayZ;
# }
# if ($rgb[0] < 0) $rgb[0] = 0;
# if ($rgb[1] < 0) $rgb[1] = 0;
# if ($rgb[2] < 0) $rgb[2] = 0;

# nParticleShape1.rgbPP = <<$rgb[0],$rgb[1],$rgb[2]>>;