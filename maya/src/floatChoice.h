 
#ifndef _floatChoice
#define _floatChoice

#include <maya/MIOStream.h>
#include <maya/MPxNode.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatArray.h>
#include <maya/MObjectArray.h>
#include "errorMacros.h"



class floatChoice : public MPxNode
{
public:
	floatChoice();
	virtual				~floatChoice();

	virtual MStatus		compute( const MPlug& plug, MDataBlock& data );
	static  void*		creator();
	static  MStatus		initialize();
	static	MTypeId		id;        
private:
	

	static  MObject   aSelector;

	 
	static  MObject   aInput0;
	static  MObject   aInput1;
	static  MObject   aInput2;
	static  MObject   aInput3;
	static  MObject   aInput4;
	static  MObject   aInput5;
	static  MObject   aInput6;
	static  MObject   aInput7;
	static  MObject   aInput8;
	static  MObject   aInput9;
	static  MObject   aInput10;
	static  MObject   aInput11;

	static MObjectArray aInputs;

	static  MObject   aOutput;


};

#endif


