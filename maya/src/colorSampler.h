/***************************************************************************
                          colorSampler.h  -  description
                             -------------------
    begin                : Wed Apr 19 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com
 ***************************************************************************/
#ifndef _colorSampler
#define _colorSampler

#include <maya/MIOStream.h>
#include <maya/MPxNode.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatArray.h>
#include "errorMacros.h"



class colorSampler : public MPxNode
{
public:
						colorSampler();
	virtual				~colorSampler();

    virtual MStatus		compute( const MPlug& plug, MDataBlock& data );
	static  void*		creator();
	static  MStatus		initialize();
	static	MTypeId		id;        

private:

	static  MObject  aDefaultLength;

	static  MObject   aPoints;
	static  MObject   aRefPoints;
	static  MObject   aUCoords;
	static  MObject   aVCoords;

	static  MObject   aNormals;
	static  MObject   aTangentUs;
	static  MObject   aTangentVs;


	static MObject   aColor;
	static MObject   aColorR;
	static MObject   aColorG;
	static MObject   aColorB;

	static  MObject   aOutColor;
	static  MObject   aOutRed;
	static  MObject   aOutGreen;
	static  MObject   aOutBlue;

	void getGenericArrayFromPlug( const MObject& plug, MDataBlock& data, MFloatPointArray &result ) ;
	void getArrayFromPlug( const MObject& plug, MDataBlock& data, MFloatPointArray &result ) ;
	void getArrayFromPlug( const MObject& plug, MDataBlock& data, MFloatVectorArray &result ) ;
	void getArrayFromPlug( const MObject& plug, MDataBlock& data, MFloatArray &result ) ;
	void getArrayFromPlug( const MObject& plug, MDataBlock& data, MFloatArray &resultX , MFloatArray &resultY)   ;
};

#endif


