/***************************************************************************
                          multiColorSampler.h  -  description
                             -------------------
    copyright            : (C) 2014 by Julian Mann
    email                : julian.mann@gmail.com
 ***************************************************************************/
#ifndef _multiColorSampler
#define _multiColorSampler

#include <maya/MIOStream.h>
#include <maya/MPxNode.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatArray.h>
#include "errorMacros.h"



class multiColorSampler : public MPxNode
{
public:
						multiColorSampler();
	virtual				~multiColorSampler();

    virtual MStatus		compute( const MPlug& plug, MDataBlock& data );
	static  void*		creator();
	static  MStatus		initialize();
	static	MTypeId		id;        


private:
	static MObject   aPosition;
	// static MObject   aPositionX;
	// static MObject   aPositionY;
	// static MObject   aPositionZ;

	static MObject   aColor;
	static MObject   aConversionFactor;
	static MObject   aOutColor;
	// static MObject   aOutColorR;
	// static MObject   aOutColorG;
	// static MObject   aOutColorB;

};

#endif


