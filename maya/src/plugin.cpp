
/*
 *  plugin.cpp
 *  jtools
 *
 *  Created by Julian Mann on 25/11/2006.
 *  Copyright 2006 hooly|mama. All rights reserved.
 *
 */

#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
#include "errorMacros.h"


#include "colorSampler.h"
#include "gradientSampler.h"
#include "multiColorSampler.h"
#include "floatChoice.h"



MStatus initializePlugin( MObject obj )
{
	
	MStatus st;
	
	MString method("initializePlugin");
	
	 MFnPlugin plugin( obj, PLUGIN_VENDOR, PLUGIN_VERSION , MAYA_VERSION);

	const MString classification( "utility/general" );

	st = plugin.registerNode( "colorSampler", colorSampler::id,colorSampler::creator,colorSampler::initialize );mser;
	st = plugin.registerNode( "gradientSampler", gradientSampler::id,gradientSampler::creator,gradientSampler::initialize );mser;
	st = plugin.registerNode( "multiColorSampler", multiColorSampler::id,multiColorSampler::creator,multiColorSampler::initialize );mser;
	st = plugin.registerNode( "floatChoice", floatChoice::id,floatChoice::creator,floatChoice::initialize ,  MPxNode::kDependNode, &classification);mser;

	MGlobal::executePythonCommandOnIdle("from culture import menu;menu.CultureMenu()", true);


	return st;
	
}



MStatus uninitializePlugin( MObject obj)
{
	MStatus st;
	
	MString method("uninitializePlugin");
	
	MFnPlugin plugin( obj );
	st = plugin.deregisterNode( floatChoice::id );mser;
	st = plugin.deregisterNode( multiColorSampler::id );mser;
	st = plugin.deregisterNode( gradientSampler::id );mser;
	st = plugin.deregisterNode( colorSampler::id );mser;

	
	return st;
}


