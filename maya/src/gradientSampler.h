

#include <maya/MIOStream.h>
#include <maya/MPxNode.h>

#include "errorMacros.h"

class gradientSampler : public MPxNode
{
public:
	gradientSampler();
	virtual ~gradientSampler();

	virtual MStatus compute(const MPlug &plug, MDataBlock &data);
	static void *creator();
	static MStatus initialize();
	static MTypeId id; // The IFF type id
private:
 
	static MObject aPoints;
	static MObject aRed;
	static MObject aSampleSize;
	static MObject aMagnitude;
	static MObject aRotation;
	static MObject aRotationMap;
	static MObject aRotationAxis;
	static MObject aRotationProjection;

	static MObject aOutput;

	enum RotationAxis { kX, kY, kZ };

};
