/***************************************************************************
colorSampler.cpp  -  description
-------------------
    begin                : Wed Apr 19 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com
	***************************************************************************/

#include <math.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MPlugArray.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MVectorArray.h>
#include <maya/MPointArray.h>
#include <maya/MFloatArray.h>

#include <maya/MVector.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFloatMatrix.h>
#include <maya/MRenderUtil.h>
#include <maya/MGlobal.h>

#include "colorSampler.h"
#include "jMayaIds.h"

MTypeId     colorSampler::id( k_colorSampler );

MObject     colorSampler::aPoints;
MObject     colorSampler::aRefPoints;
MObject     colorSampler::aUCoords;
MObject     colorSampler::aVCoords;

MObject     colorSampler::aNormals;
MObject     colorSampler::aTangentUs;
MObject     colorSampler::aTangentVs;

MObject     colorSampler::aColor;
MObject     colorSampler::aColorR;
MObject     colorSampler::aColorG;
MObject     colorSampler::aColorB;
// output to particles
MObject     colorSampler::aOutColor;
MObject     colorSampler::aOutRed;
MObject     colorSampler::aOutGreen;
MObject     colorSampler::aOutBlue;

MObject     colorSampler::aDefaultLength;


colorSampler::colorSampler() {}
colorSampler::~colorSampler() {}

void* colorSampler::creator()
{
	return new colorSampler();
}
MStatus colorSampler::initialize()
{
	MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;


	aDefaultLength = nAttr.create( "defaultLength", "dln", MFnNumericData::kInt);
	nAttr.setStorable(true);
	nAttr.setHidden(false);
	nAttr.setWritable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault(-1);
	
	addAttribute(aDefaultLength);

	aColorR = nAttr.create( "colorR", "cr", MFnNumericData::kFloat);
	aColorG = nAttr.create( "colorG", "cg", MFnNumericData::kFloat);
	aColorB = nAttr.create( "colorB", "cb", MFnNumericData::kFloat);
	aColor  = nAttr.create( "color",  "c" , aColorR, aColorG, aColorB);
	nAttr.setStorable(true);
	nAttr.setHidden(false);
	nAttr.setWritable(true);
	nAttr.setUsedAsColor(true);
	addAttribute(aColor);
	
	aPoints =  tAttr.create("points","pts",MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	addAttribute(aPoints);

	aRefPoints =  tAttr.create("refPoints","rpts",MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	addAttribute(aRefPoints)  ;

	aUCoords =  tAttr.create("uCoords","ucs",MFnData::kDoubleArray);
	tAttr.setReadable(false);
	tAttr.setStorable(false);
	addAttribute(aUCoords)    ;
	
	aVCoords =  tAttr.create("vCoords","vcs",MFnData::kDoubleArray);
	tAttr.setReadable(false);
	tAttr.setStorable(false);
	addAttribute(aVCoords)    ;

	aNormals =  tAttr.create("normals","nms",MFnData::kVectorArray);
	tAttr.setReadable(false);
	tAttr.setStorable(false);
	addAttribute(aNormals)   ;
	
	aTangentUs =  tAttr.create("tangentUs","tus",MFnData::kDoubleArray);
	tAttr.setReadable(false);
	tAttr.setStorable(false);
	addAttribute(aTangentUs)  ;
	
	aTangentVs =  tAttr.create("tangentVs","tvs",MFnData::kDoubleArray);
	tAttr.setReadable(false);
	tAttr.setStorable(false);
	addAttribute(aTangentVs)  ;
	
	aOutColor	=  tAttr.create("outColor","oc",MFnData::kVectorArray);
	tAttr.setWritable(false);
	tAttr.setReadable(true);
	tAttr.setStorable(false);
	addAttribute(aOutColor);
	
	aOutRed	=  tAttr.create("outRed","or",MFnData::kDoubleArray);
	tAttr.setWritable(false);
	tAttr.setReadable(true);
	tAttr.setStorable(false);
	addAttribute(aOutRed);
	
	aOutGreen	=  tAttr.create("outGreen","og",MFnData::kDoubleArray);
	tAttr.setWritable(false);
	tAttr.setReadable(true);
	tAttr.setStorable(false);
	addAttribute(aOutGreen);
	
	aOutBlue	=  tAttr.create("outBlue","ob",MFnData::kDoubleArray);
	tAttr.setWritable(false);
	tAttr.setReadable(true);
	tAttr.setStorable(false);
	addAttribute(aOutBlue);
	
	
	attributeAffects(aColor, aOutColor);
	attributeAffects(aColorR, aOutColor);
	attributeAffects(aColorG, aOutColor);
	attributeAffects(aColorB, aOutColor);
	attributeAffects(aPoints, aOutColor);
	attributeAffects(aRefPoints, aOutColor);
	attributeAffects(aUCoords, aOutColor);
	attributeAffects(aVCoords, aOutColor);
	attributeAffects(aNormals, aOutColor);
	attributeAffects(aTangentUs, aOutColor);
	attributeAffects(aTangentVs, aOutColor);
	attributeAffects(aDefaultLength,aOutColor);

	attributeAffects(aColor, aOutRed);
	attributeAffects(aColorR, aOutRed);
	attributeAffects(aPoints, aOutRed);
	attributeAffects(aRefPoints, aOutRed);
	attributeAffects(aUCoords, aOutRed);
	attributeAffects(aVCoords, aOutRed);
	attributeAffects(aNormals, aOutRed);
	attributeAffects(aTangentUs, aOutRed);
	attributeAffects(aTangentVs, aOutRed);
	attributeAffects(aDefaultLength,aOutRed);

	attributeAffects(aColor, aOutGreen);
	attributeAffects(aColorG, aOutGreen);
	attributeAffects(aPoints, aOutGreen);
	attributeAffects(aRefPoints, aOutGreen);
	attributeAffects(aUCoords, aOutGreen);
	attributeAffects(aVCoords, aOutGreen);
	attributeAffects(aNormals, aOutGreen);
	attributeAffects(aTangentUs, aOutGreen);
	attributeAffects(aTangentVs, aOutGreen);
	attributeAffects(aDefaultLength,aOutGreen);

	attributeAffects(aColor, aOutBlue);
	attributeAffects(aColorB, aOutBlue);
	attributeAffects(aPoints, aOutBlue);
	attributeAffects(aRefPoints, aOutBlue);
	attributeAffects(aUCoords, aOutBlue);
	attributeAffects(aVCoords, aOutBlue);
	attributeAffects(aNormals, aOutBlue);
	attributeAffects(aTangentUs, aOutBlue);
	attributeAffects(aTangentVs, aOutBlue);
	attributeAffects(aDefaultLength,aOutBlue);

	
	return MS::kSuccess;
}


void colorSampler::getArrayFromPlug( const MObject& plug, MDataBlock& data, MFloatPointArray &result ) {
	MStatus st;
	MString method("colorSampler::getArrayFromPlug");
	MDataHandle h = data.inputValue( plug , &st);
	if (st == MS::kSuccess) {
		MObject d = h.data();
		MFnVectorArrayData fn( d );
		MVectorArray array = fn.array( &st ); 
		if (st == MS::kSuccess) { // convert vectors to floatPoints
			result.setLength(array.length())    ;
			for (unsigned i = 0;i < array.length();i++) {
				result[i] =  MFloatPoint(float(array[i].x) , float(array[i].y) ,float(array[i].z) );
			}
		}
	}
}
void colorSampler::getArrayFromPlug( const MObject& plug, MDataBlock& data, MFloatVectorArray &result ) {
	MStatus st;
	MString method("colorSampler::getArrayFromPlug");
	MDataHandle h = data.inputValue( plug , &st);  
	if (st == MS::kSuccess) {
		MObject d = h.data();
		MFnVectorArrayData fn( d );
		MVectorArray array = fn.array( &st ); 
		if (st == MS::kSuccess) { // convert vectors to floatVectors
			result.setLength(array.length())  ;
			for (unsigned i = 0;i < array.length();i++) {
				result[i] = MFloatVector(float(array[i].x) , float(array[i].y) ,float(array[i].z) );

			}
		}
	}
}
void colorSampler::getArrayFromPlug( const MObject& plug, MDataBlock& data, MFloatArray &result) {
	MStatus st;
	MString method("colorSampler::getArrayFromPlug");
	MDataHandle h = data.inputValue( plug , &st);  
	if (st == MS::kSuccess) {
		MObject d = h.data();
		MFnDoubleArrayData fn( d );
		MDoubleArray array = fn.array( &st );   
		if (st == MS::kSuccess) { // convert vectors to floatVectors
			result.setLength(array.length()) ;
			for (unsigned i = 0;i < array.length();i++) {
				result[i] = float(array[i]);
			}
		}
	}
}

void colorSampler::getArrayFromPlug( const MObject& plug, MDataBlock& data, MFloatArray &resultX , MFloatArray &resultY) {
	MStatus st;
	MString method("colorSampler::getArrayFromPlug");
	MDataHandle h = data.inputValue( plug , &st);  
	if (st == MS::kSuccess) {
		MObject d = h.data();
		MFnVectorArrayData fn( d );
		MVectorArray array = fn.array( &st );   
		if (st == MS::kSuccess) { // convert vectors to floatVectors
			resultX.setLength(array.length());
			resultY.setLength(array.length()) ;
			for (unsigned i = 0;i < array.length();i++) {
				resultX[i] = float(array[i].x);
				resultY[i] = float(array[i].y);
			}
		}
	}
}

MStatus colorSampler::compute( const MPlug& plug, MDataBlock& data )
{
	
	MStatus st;
	MString method("colorSampler::compute");
	
	if(! (
		(plug == aOutColor) ||
		(plug == aOutRed) ||
		(plug == aOutGreen) ||
		(plug == aOutBlue) 
		) ) return MS::kUnknownParameter;
	//cerr << "texSampler 1"  << endl;
		// if (plug.name() == "aliveSampler.outRed") {
		// 	cerr << "plug"  << plug.name() << endl;
		// }

		int defaultLength = data.inputValue(aDefaultLength).asInt();

	// connected texture
	///////////////////////////////////////
		MPlugArray plugArray;
		MFloatVector color = data.inputValue( aColor ).asFloatVector();
		MPlug colorPlug(thisMObject(), aColor);
		bool hasConnections = colorPlug.connectedTo(plugArray,1,0,&st); 
		MString name;
		if (hasConnections) name = plugArray[0].name(&st);
	///////////////////////////////////////

		MFloatMatrix cameraMat;
		cameraMat.setToIdentity();

		MFloatPointArray pVals;
		MFloatPointArray rpVals;
		MFloatArray uVals;
		MFloatArray vVals;
		MFloatVectorArray nVals;
		MFloatVectorArray tuVals;
		MFloatVectorArray tvVals;

	//cerr << "texSampler 3"  << endl;

	int ns = 0 ; // num samples
	getArrayFromPlug( aPoints,  data,  pVals ) ;
	getArrayFromPlug( aRefPoints,  data,  rpVals )   ;
	// getArrayFromPlug( aUVCoords,  data,  uVals, vVals )    ;
	getArrayFromPlug( aUCoords,  data,  uVals )    ;
	getArrayFromPlug( aVCoords,  data,  vVals )    ;
	
	getArrayFromPlug( aNormals,  data,  nVals )      ;
	getArrayFromPlug( aTangentUs,  data,  tuVals )   ;
	getArrayFromPlug( aTangentVs,  data,  tvVals )   ;
	
	
	// work out how many samples (use the first non zero array length)
	if (pVals.length()) 
	{
		ns = pVals.length();
	}
	else if (rpVals.length()) 
	{
		ns = rpVals.length() ;
	}
	else if (uVals.length()) 
	{
		ns = uVals.length() ;
	}
	else if (vVals.length()) 
	{
		ns = vVals.length();
	}
	else if (nVals.length()) 
	{
		ns = nVals.length() ;
	}
	else if (tuVals.length()) 
	{
		ns = tuVals.length() ;
	}
	else if (tvVals.length()) 
	{
		ns = tvVals.length() ;
	}
	//cerr << "texSampler 5"  << endl;
	
	MVectorArray outColor;
	MDoubleArray outRed;
	MDoubleArray outGreen;
	MDoubleArray outBlue;

	// if (plug.name() == "aliveSampler.outRed") {
	// 	cerr << "ns " << ns << endl;
	// 	cerr << "pVals: " << pVals << endl;
	// }

	// DBG
	if (ns) {
		// DBG
		if (hasConnections) {
			MFloatVectorArray resColors;
			MFloatVectorArray resTransparencies;

			st =  MRenderUtil::sampleShadingNetwork (
				name ,
				ns,
				false,
				false,
				cameraMat ,
				(pVals.length()>0) 		? &pVals : 0,
				(uVals.length()>0) 		? &uVals : 0,
				(vVals.length()>0) 		? &vVals : 0,
				(nVals.length()>0) 		? &nVals : 0,
				(rpVals.length()>0) 		? &rpVals : 0,
				(tuVals.length()>0) 		? &tuVals : 0,
				(tvVals.length()>0) 		? &tvVals : 0,
				0,
				 resColors,  // Then this array will hold results
				 resTransparencies
				 );

			if (! st.error()) {
				outColor.setLength(ns);
				outRed.setLength(ns);
				outGreen.setLength(ns);
				outBlue.setLength(ns);

				for(int i = 0;i<ns;i++) {
					outColor.set( resColors[i] ,i );
					outRed.set( resColors[i].x ,i );
					outGreen.set( resColors[i].y ,i );
					outBlue.set( resColors[i].z ,i );
				}
			}
		} else {
			outColor.setLength(ns);
			outRed.setLength(ns);
			outGreen.setLength(ns);
			outBlue.setLength(ns);
			for(int i = 0;i<ns;i++) {
				outColor.set( MVector(color.x, color.y, color.z) ,i );
				outRed.set( double(color.x) ,i );
				outGreen.set( double(color.y) ,i );
				outBlue.set( double(color.z) ,i );
			}
		}
	}


	if ((defaultLength != -1) && ( defaultLength != ns)) {
		outColor.setLength(defaultLength);
		outRed.setLength(defaultLength );
		outGreen.setLength(  defaultLength );
		outBlue.setLength( defaultLength);
		if (defaultLength > ns) {
			for (int i = ns; i < defaultLength; ++i)
			{
		 	  outColor.set( MVector::zero ,i );
				outRed.set( 0.0 ,i );
				outGreen.set(0.0 ,i );
				outBlue.set( 0.0 ,i );
			}
		}
	}


	//cerr << "texSampler 9"  << endl;

		// DBG
	MDataHandle hOutColor = data.outputValue(aOutColor, &st ); mser;
	MFnVectorArrayData fnOutColor;
	MObject dOutColor = fnOutColor.create(outColor , &st );    mser;
	hOutColor.set( dOutColor );
	data.setClean( aOutColor );
	// if (plug.name() == "aliveSampler.outRed") {		
	// 	cerr <<  "outRed: " << outRed << endl;
	// }
	MDataHandle hOutRed = data.outputValue(aOutRed, &st ); mser;
	MFnDoubleArrayData fnOutRed;
	MObject dOutRed = fnOutRed.create(outRed , &st );    mser;
	hOutRed.set( dOutRed );
	data.setClean( aOutRed );

	MDataHandle hOutGreen = data.outputValue(aOutGreen, &st ); mser;
	MFnDoubleArrayData fnOutGreen;
	MObject dOutGreen = fnOutGreen.create(outGreen , &st );    mser;
	hOutGreen.set( dOutGreen );
	data.setClean( aOutGreen );

	MDataHandle hOutBlue = data.outputValue(aOutBlue, &st ); mser;
	MFnDoubleArrayData fnOutBlue;
	MObject dOutBlue = fnOutBlue.create(outBlue , &st );    mser;
	hOutBlue.set( dOutBlue );
	data.setClean( aOutBlue );

	//cerr << "texSampler 10"  << endl;
	
	return MS::kSuccess;
}

