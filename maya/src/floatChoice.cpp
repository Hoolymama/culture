 
#include <math.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MPlugArray.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFloatPointArray.h>
#include <maya/MObjectArray.h>
// #include <maya/MVectorArray.h>
// #include <maya/MPointArray.h>
// #include <maya/MFloatArray.h>

#include <maya/MVector.h>
// #include <maya/MFnDoubleArrayData.h>
// #include <maya/MFnVectorArrayData.h>
// #include <maya/MFnPointArrayData.h>
#include <maya/MFloatMatrix.h>
#include <maya/MRenderUtil.h>
#include <maya/MGlobal.h>
#include <errorMacros.h>
#include "floatChoice.h"
#include "jMayaIds.h"

MTypeId     floatChoice::id( k_floatChoice );


MObject     floatChoice::aSelector;

MObject     floatChoice::aInput0;
MObject     floatChoice::aInput1;
MObject     floatChoice::aInput2;
MObject     floatChoice::aInput3;
MObject     floatChoice::aInput4;
MObject     floatChoice::aInput5;
MObject     floatChoice::aInput6;
MObject     floatChoice::aInput7;
MObject     floatChoice::aInput8;
MObject     floatChoice::aInput9;
MObject     floatChoice::aInput10;
MObject     floatChoice::aInput11;

MObject     floatChoice::aOutput;

MObjectArray     floatChoice::aInputs;


floatChoice::floatChoice() {}
floatChoice::~floatChoice() {}

void* floatChoice::creator()
{
	return new floatChoice();
}
MStatus floatChoice::initialize()
{
	MStatus st;

	MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;


	aSelector = nAttr.create( "selector", "s", MFnNumericData::kInt);
	nAttr.setStorable(true);
	nAttr.setHidden(false);
	nAttr.setWritable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault(0);
	st =  addAttribute(aSelector);


	// MObjectArray atts;
	aInputs.append(aInput0);
	aInputs.append(aInput1);
	aInputs.append(aInput2);
	aInputs.append(aInput3);
	aInputs.append(aInput4);
	aInputs.append(aInput5);
	aInputs.append(aInput6);
	aInputs.append(aInput7);
	aInputs.append(aInput8);
	aInputs.append(aInput9);
	aInputs.append(aInput10);
	aInputs.append(aInput11);
	
	MString longName("input");
	MString shortName("i");


  aOutput =  nAttr.create("output","out",MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setReadable(true);
	nAttr.setStorable(false);
	st = addAttribute(aOutput);
	for (unsigned i = 0; i< 12 ; i++) {
		aInputs[i] = nAttr.create((longName+i), (shortName+i), MFnNumericData::kFloat);
		nAttr.setStorable(true);		
		nAttr.setReadable(true); 
		nAttr.setWritable(true);
		st = addAttribute(aInputs[i]); 
		attributeAffects(aInputs[i],aOutput); 
	}

	attributeAffects(aSelector, aOutput);

	return MS::kSuccess;
}

MStatus floatChoice::compute( const MPlug& plug, MDataBlock& data )
{

 

	MStatus st;
	MString method("floatChoice::compute");
	// cerr << method << endl;

	if	(plug != aOutput)  return MS::kUnknownParameter;




	int index = data.inputValue(aSelector).asInt();

	float result = 0;

	if (index < aInputs.length()) {
		result=data.inputValue(aInputs[index]).asFloat();
	}

 //  result=data.inputValue(aInput0).asFloat();

	// if (index == 10) {
	// 	result=data.inputValue(aInput0).asFloat();
	// } else if (index == 11) {
	// 	result=data.inputValue(aInput1).asFloat();
	// }  
 
 //    MFloatVector& anormalcamera = data.inputValue( aNormalCamera ).asFloatVector();
 //   MFloatVector& apointcamera = data.inputValue( aPointCamera ).asFloatVector();
 //   MFloatVector& apointworld = data.inputValue( aPointWorld ).asFloatVector();

 // const float2 &  auvcoord= data.inputValue( aUVCoord).asFloat2();


	// MArrayDataHandle hInput = data.inputArrayValue( aInput, &st );mser;
	// unsigned nPlugs = hInput.elementCount();

	// st = hInput.jumpToElement (index);
	// if (st == MS::kSuccess) {
	// 	result= hInput.inputValue().asFloat();
	// }

	MDataHandle outputHandle = data.outputValue( aOutput );
	float& out = outputHandle.asFloat();
	out = result;
	outputHandle.setClean();

	return MS::kSuccess;
}

