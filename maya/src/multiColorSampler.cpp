/***************************************************************************
multiColorSampler.cpp  -  description
-------------------
    begin                : Wed Apr 19 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com
	***************************************************************************/

#include <math.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MPlugArray.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MVectorArray.h>
#include <maya/MPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MArrayDataBuilder.h>

#include <maya/MDataHandle.h>
#include <maya/MVector.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFloatMatrix.h>
#include <maya/MRenderUtil.h>
#include <maya/MGlobal.h>

#include "multiColorSampler.h"
#include "jMayaIds.h"

MTypeId     multiColorSampler::id( k_multiColorSampler );

MObject     multiColorSampler::aPosition;
// MObject     multiColorSampler::aPositionX;
// MObject     multiColorSampler::aPositionY;
// MObject     multiColorSampler::aPositionZ;

MObject     multiColorSampler::aColor;
MObject     multiColorSampler::aConversionFactor;
MObject     multiColorSampler::aOutColor;
// MObject     multiColorSampler::aOutColorR;
// MObject     multiColorSampler::aOutColorG;
// MObject     multiColorSampler::aOutColorB;

multiColorSampler::multiColorSampler() {}
multiColorSampler::~multiColorSampler() {}

void* multiColorSampler::creator()
{
	return new multiColorSampler();
}

MStatus multiColorSampler::initialize()
{
	MStatus st;
	MFnNumericAttribute nAttr;

	aColor = nAttr.createColor( "color", "col");
    nAttr.setStorable(true);
    nAttr.setHidden(false);
    nAttr.setWritable(true);
    nAttr.setUsedAsColor(true);
	st = addAttribute(aColor); mser;
	
	// aPositionX =  nAttr.create( "positionX", "posx", MFnNumericData::kFloat );
	// nAttr.setStorable(true);
	// nAttr.setWritable(true);

	// aPositionY =  nAttr.create( "positionY", "posy", MFnNumericData::kFloat );
	// nAttr.setStorable(true);
	// nAttr.setWritable(true);

	// aPositionZ =  nAttr.create( "positionZ", "posz", MFnNumericData::kFloat );
	// nAttr.setStorable(true);
	// nAttr.setWritable(true);
	
	aPosition =  nAttr.createPoint( "position", "pos");
	nAttr.setArray(true);
	nAttr.setStorable(true);
	nAttr.setWritable(true);
	nAttr.setDisconnectBehavior(MFnAttribute::kDelete);
	st = addAttribute(aPosition);mser;


	aConversionFactor = nAttr.create("conversionFactor", "cvf", MFnNumericData::kFloat);
	nAttr.setWritable(true);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault(1.0);
	st= addAttribute(aConversionFactor);mser;

	// aOutColorR	=  nAttr.create("outColorR","ocr", MFnNumericData::kFloat);
	// nAttr.setReadable(true);
	// nAttr.setStorable(false);
	// aOutColorG	=  nAttr.create("outColorG","ocg", MFnNumericData::kFloat);
	// nAttr.setReadable(true);
	// nAttr.setStorable(false);
	// aOutColorB	=  nAttr.create("outColorB","ocb", MFnNumericData::kFloat);
	// nAttr.setReadable(true);
	// nAttr.setStorable(false);

	// aOutColor	=  nAttr.createPoint("outColor","oc",aOutColorR,aOutColorG,aOutColorB);
	// nAttr.setArray(true);
	// nAttr.setUsesArrayDataBuilder( true );
	// st= addAttribute(aOutColor);mser;
	
	aOutColor	=  nAttr.createColor("outColor","oc");
	nAttr.setArray(true);
	nAttr.setReadable(true);
	nAttr.setStorable(false);
	nAttr.setUsesArrayDataBuilder( true );
	st= addAttribute(aOutColor);mser;
	

	attributeAffects(aColor, aOutColor);
	attributeAffects(aPosition, aOutColor);
	// attributeAffects(aPositionX, aOutColor);
	// attributeAffects(aPositionY, aOutColor);
	// attributeAffects(aPositionZ, aOutColor);
	attributeAffects(aConversionFactor, aOutColor);
	
	// attributeAffects(aColor, aOutColorR);
	// attributeAffects(aPosition, aOutColorR);
	// attributeAffects(aPositionX, aOutColorR);
	// attributeAffects(aPositionY, aOutColorR);
	// attributeAffects(aPositionZ, aOutColorR);
	// attributeAffects(aConversionFactor, aOutColorR);
	
	// attributeAffects(aColor, aOutColorG);
	// attributeAffects(aPosition, aOutColorG);
	// attributeAffects(aPositionX, aOutColorG);
	// attributeAffects(aPositionY, aOutColorG);
	// attributeAffects(aPositionZ, aOutColorG);
	// attributeAffects(aConversionFactor, aOutColorG);
	
	// attributeAffects(aColor, aOutColorB);
	// attributeAffects(aPosition, aOutColorB);
	// attributeAffects(aPositionX, aOutColorB);
	// attributeAffects(aPositionY, aOutColorB);
	// attributeAffects(aPositionZ, aOutColorB);
	// attributeAffects(aConversionFactor, aOutColorB);
	
	return MS::kSuccess;
}

MStatus multiColorSampler::compute( const MPlug& plug, MDataBlock& data )
{
	
	MStatus st;
	MString method("multiColorSampler::compute");
	MPlug thePlug = plug;
	if (thePlug.isChild()) thePlug = thePlug.parent();
	if(thePlug != aOutColor) return MS::kUnknownParameter;
	JPMDBG;

	// get connected texture
	///////////////////////////////////////
	MPlugArray plugArray;
	MFloatVector defaultColor = data.inputValue( aColor ).asFloatVector();
	MPlug colorPlug(thisMObject(), aColor);
	bool hasConnection = colorPlug.connectedTo(plugArray,1,0,&st); 
	MString name;
	if (hasConnection) name = plugArray[0].name(&st);
	///////////////////////////////////////
	JPMDBG;

	// get positions
	//////////////////////////////////////////////////////////////////////
	MArrayDataHandle hPositions = data.inputArrayValue( aPosition , &st);  msert;
	unsigned int count = hPositions.elementCount(&st); msert;
	MFloatPointArray pArray;
	MIntArray sparseIndices;
	do {
		unsigned int el = hPositions.elementIndex(&st);  
		if (st == MS::kSuccess) {
			MDataHandle hPosition = hPositions.inputValue( &st );mser;	
			const MFloatVector & v = hPosition.asFloatVector();
			pArray.append(MFloatPoint(v.x, v.y, v.z));
			sparseIndices.append(el);
		}
	} while (hPositions.next() == MS::kSuccess );
	//////////////////////////////////////////////////////////////////////
	JPMDBG;

	float conversion = data.inputValue( aConversionFactor ).asFloat();
	JPMDBG;

	MFloatMatrix cameraMat;
	cameraMat.setToIdentity();
	int numSamples = pArray.length() ; // num samples
	MFloatVectorArray resColors;

		JPMDBG;

	if (numSamples) {
		if (hasConnection) {
			MFloatVectorArray resTransparencies;
			st =  MRenderUtil::sampleShadingNetwork (
				 name ,
				 numSamples,
				 false,
				 false,
				 cameraMat ,
				 &pArray ,
				 0,
				 0,
				 0,
				 &pArray,
				 0,
				 0,
				 0,
				 resColors, 
				 resTransparencies
			);
		} else {
			resColors = MFloatVectorArray(numSamples,defaultColor);
		}
	}
	JPMDBG;

	// do the arrayDataBuilder malarkey
	MArrayDataHandle     hOutput = data.outputArrayValue( aOutColor, &st ); msert;
	MArrayDataBuilder    bOutput = hOutput.builder();	

	if (conversion != 1.0f) {
		for( unsigned i = 0;i<numSamples;i++) {
			resColors[i] = resColors[i] * conversion;
		}
	}
	JPMDBG;

	// So we use sparse indices to make sure 
	// we update the same output plug id as the input. 
	for( unsigned i = 0;i<numSamples;i++) {
		MDataHandle hOut = bOutput.addElement(sparseIndices[i]);
		float3 &out = hOut.asFloat3();
		out[0] = resColors[i].x;
		out[1] = resColors[i].y;
		out[2] = resColors[i].z;
	}
	JPMDBG;
	hOutput.setAllClean();
	JPMDBG;
	return MS::kSuccess;
}

