// gradientSampler
// Julian Mann
// March 2001
#include <math.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MMatrix.h>
#include <maya/MAngle.h>

#include <maya/MQuaternion.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnEnumAttribute.h>

#include <maya/MFloatPointArray.h> 
#include <maya/MVectorArray.h>
#include <maya/MFnVectorArrayData.h> 
#include "gradientSampler.h"
#include "jMayaIds.h"
#include "texUtils.h"


MTypeId     gradientSampler::id( k_gradientSampler  );

MObject     gradientSampler::aPoints; 
MObject     gradientSampler::aRed;
MObject     gradientSampler::aSampleSize;
MObject     gradientSampler::aMagnitude;

MObject     gradientSampler::aRotation;
MObject     gradientSampler::aRotationMap;
MObject     gradientSampler::aRotationProjection;
MObject     gradientSampler::aRotationAxis;


MObject     gradientSampler::aOutput;

gradientSampler::gradientSampler() {}
gradientSampler::~gradientSampler() {}

void* gradientSampler::creator()
{
	return new gradientSampler();
}
MStatus gradientSampler::initialize()
{
	MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnUnitAttribute uAttr;
	MFnMatrixAttribute mAttr;
	MFnEnumAttribute eAttr;

	aPoints =  tAttr.create("points","pts",MFnData::kVectorArray);
	tAttr.setReadable(false);
	addAttribute(aPoints);


    aRed  = nAttr.create( "red",  "rd" , MFnNumericData::kFloat);
    nAttr.setStorable(true);
    nAttr.setHidden(false);
    nAttr.setWritable(true);
	addAttribute(aRed);

	aSampleSize = nAttr.create( "sampleSize", "ss", MFnNumericData::kFloat);
	addAttribute(aSampleSize);

	aMagnitude = nAttr.create( "magnitude", "mg", MFnNumericData::kFloat);
	addAttribute(aMagnitude);


	aRotation = uAttr.create("rotation", "ro", MFnUnitAttribute::kAngle);
	uAttr.setHidden(false);
	uAttr.setKeyable(true);
	uAttr.setStorable(true);
	addAttribute(aRotation);

	aRotationMap = nAttr.create( "rotationMap", "rm", MFnNumericData::kFloat);
	addAttribute(aRotationMap);



	aRotationAxis= eAttr.create("rotationAxis","rax");
	eAttr.addField( "xAxis"	,  gradientSampler::kX     );
	eAttr.addField( "yAxis"	,  gradientSampler::kY     );
	eAttr.addField( "zAxis"	,  gradientSampler::kZ     );
	eAttr.setDefault( gradientSampler::kZ );
	eAttr.setHidden( false );
	eAttr.setKeyable( true );
	addAttribute( aRotationAxis );  


	MMatrix identity;
	identity.setToIdentity();
	aRotationProjection = mAttr.create("rotationProjection", "rprj", MFnMatrixAttribute::kDouble);
	mAttr.setStorable(false);
	mAttr.setHidden(true);
	mAttr.setDefault(identity);
	addAttribute(aRotationProjection);
	
	aOutput	=  tAttr.create("output","out",MFnData::kVectorArray);
	tAttr.setWritable(false);
	tAttr.setReadable(true);
	tAttr.setStorable(false);
	addAttribute(aOutput);
 
	attributeAffects(aPoints, aOutput);
	attributeAffects(aRed, aOutput);
	attributeAffects(aSampleSize, aOutput);
	attributeAffects(aMagnitude, aOutput);
	
	attributeAffects(aRotation, aOutput);
	attributeAffects(aRotationMap, aOutput);
	attributeAffects(aRotationAxis, aOutput);
	attributeAffects(aRotationProjection, aOutput);

	return MS::kSuccess;
}

MStatus gradientSampler::compute( const MPlug& plug, MDataBlock& data )
{
	
	if( plug != aOutput) return MS::kUnknownParameter;

	MStatus st;
	MObject thisObj = thisMObject();
 
	if (! TexUtils::hasTexture(thisObj, gradientSampler::aRed))
	{
		return MS::kUnknownParameter;
	}
 
	MDataHandle hPoints = data.inputValue( aPoints , &st); msert;
	MObject dPoints = hPoints.data();
	MFnVectorArrayData fnPoints( dPoints );
	MVectorArray points = fnPoints.array( &st );msert;

	unsigned num = points.length();
	MFloatPointArray fPoints(num);
	for (size_t i = 0; i < num; i++)
	{
		fPoints[i]=MFloatPoint(points[i]);
	}
	
	MFloatArray sampleDistances;
	TexUtils::defaultedSampleSolidTexture(thisObj, gradientSampler::aSampleSize, fPoints, sampleDistances);

	MFloatVectorArray gradients;
	st = TexUtils::sample3dGradient(
		thisObj,
		gradientSampler::aRed,
		fPoints,
		sampleDistances,
		gradients);msert;
 
	MFloatArray magnitudes;
	TexUtils::defaultedSampleSolidTexture(thisObj,gradientSampler::aMagnitude, fPoints,magnitudes);
	

	MMatrix mat = data.inputValue(gradientSampler::aRotationProjection).asMatrix().inverse();
	RotationAxis rAxis = (RotationAxis)data.inputValue(aRotationAxis).asShort();
	MVector axis;
	if (rAxis == gradientSampler::kX)
	{
		axis = MVector::xAxis * mat;
	} else if (rAxis == gradientSampler::kY)
	{
		axis = MVector::yAxis * mat;
	} else {
		axis = MVector::zAxis * mat;
	}

	MVectorArray output(num) ;
	// axis.normalize();
  	float rotation =  float(data.inputValue(aRotation).asAngle().asRadians());
	MFloatArray rotations;
	TexUtils::defaultedSampleSolidTexture(thisObj,gradientSampler::aRotationMap, fPoints, rotations);
	for (size_t i = 0; i < num; i++)
	{
		double angle = rotations[i] * rotation;
		MQuaternion q(angle,axis);
		output[i] = MVector(gradients[i]).rotateBy(q) * magnitudes[i];
	}

	MDataHandle hOutput = data.outputValue(gradientSampler::aOutput, &st );mser;
	MFnVectorArrayData fnOutput;
	MObject dOutput = fnOutput.create(output , &st ); mser;
	hOutput.set( dOutput );
	data.setClean( aOutput );


	return MS::kSuccess;
}			
